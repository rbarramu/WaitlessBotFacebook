'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const request = require('request')
const app = express()
//var qr = require('qr-image');
var rutUser = ''

//Guardo los servicios y compañias
var optionServices = []
var optionCompanies = []
var optionBranch = []
var optionQueue =[]

var idSucursal = 0
var idEmpresa = 0
var idCola = 0


var idesqueue = []
var arrayIdAttention = []
var idColaValorar = 0
var id_Attention = 0
var idAttention = 0


app.set('port', (process.env.PORT || 5000))

// Process application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}))

// Process application/json
app.use(bodyParser.json())

// Index route
app.get('/', function (req, res) {
    res.send('Hello world, I am a chat bot')
})

// for Facebook verification
app.get('/webhook/', function (req, res) {
    if (req.query['hub.verify_token'] === 'WaitlessBot_verify_token') {
        res.send(req.query['hub.challenge'])
    }
    res.send('Error, wrong token')
})



// Spin up the server
app.listen(app.get('port'), function() {
    console.log('running on port', app.get('port'))
})


var json_response = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": []
    }
  }
}

app.post('/webhook/', function name(req, res) {
    let messaging_events = req.body.entry[0].messaging


    for (let i = 0; i < messaging_events.length; i++) {
        let event = req.body.entry[0].messaging[i]

        let sender = event.sender.id


        if (event.message && event.message.text) {
            let text = event.message.text

            if (isNaN (text)){

              var rut = text.substring(0, 200);

              if (rut.indexOf("-")==-1)

                {

                for (i = 0; i < optionServices.length; i++) {
                  if (text == optionServices[i].name){
                      var id = optionServices[i].id

                      //getCompanies(sender, id)
                      getCompanies(sender,id,function(companies){
                        //  console.log(companies);

                          var cantCompanies = companies.length
                          generarBotonesCompanies(sender,cantCompanies,companies)
                      })
                  }
                }
                for (i = 0; i < optionCompanies.length; i++) {
                  if (text == optionCompanies[i].name){
                       idEmpresa = optionCompanies[i].id

                      //getCompanies(sender, id)
                      getSucursales(sender,function(sucursales){
                          //console.log(sucursales);

                          var cantSucursales = sucursales.length
                          generarBotonesSucursales(sender,cantSucursales,sucursales)
                      })
                  }
                }
                for (i = 0; i < optionBranch.length; i++) {
                  if (text == optionBranch[i].name){

                      idSucursal = optionBranch[i].id
                      idEmpresa = optionBranch[i].company_id




                        getColas(sender, function(colas){
                          var cantColas= colas.length
                          generarBotonesColas(sender,cantColas,colas)


                      })


                  }
                }



                for (i = 0; i < optionQueue.length; i++) {
                  if (text == optionQueue[i].name){
                       idCola = optionQueue[i].id
                        //   console.log(idCola)



                      getDatosColas(sender,function(name,current_number,last_number){
                        //console.log(name)

                        revisarRut(rutUser, function(value,status){
                          //console.log(status)


                          if (status == "waiting"){
                            waiting(sender)
                          }
                          if (status == "attending"){
                            sendTextMessage(sender,"Es tu turno, dirígete al módulo!\n ")
                          }

                          if (status == "notattending"){
                            notattending(sender)
                          }
                          if (status == "ready"){
                            ready(sender)
                          }

                          if (status == "No existe"){
                              sendGenericMessage(sender)
                          }
                        })
                  })
                }
              }

            /*  for (var j = 0; j < optionMisQueues.length; j++) {
              var empresa = optionMisQueues[j].nameCompany

              console.log(optionMisQueues[j].nameCompany)

                if (text == empresa){

                  idCola = optionMisQueues[j].queue_id
                  idAttention = optionMisQueues[j].id

                  console.log(idAttention)
                    getDatosColas(sender,function(name,current_number,last_number){

                          waiting(sender)

                      })
                }
              }*/


                  //sendTextMessage(sender,"No ingresaste un rut.\nIngresa tu Rut, sin puntos y con guión (ejemplo: 12345678-9)" )
                }
              else {
                var rutValidado = Fn.validaRut(rut)
                if (rutValidado == true ) {
                    rutUser = rut.replace("-","");
                    if (rutUser.charAt(8)=='k'){
                      rutUser = rutUser.replace("k","0")
                    }
                    if (rutUser.charAt(8)=='K'){
                      rutUser= rutUser.replace("K","0")
                      console.log(rutUser)
                    }
                    opcionBienvenida(sender)


                    } else {

                       sendTextMessage(sender,"Ingresaste un rut inválido.\nIngresa tu Rut, sin puntos y con guión (ejemplo: 12345678-9)")
                    }

                //Fn.validaRut( text.substring(0, 200)) ?   sendGenericMessage(sender,rut) :   sendTextMessage(sender,"Ingresaste un rut inválido.\nIngresa tu Rut, sin puntos y con guión (ejemplo: 18229671-6)");
              }
            }

              else{
                if(text=="1"){
                  var rate = 1
                console.log(rate)

                revisarMisColas(rutUser, function(misColas,status){

                  console.log(misColas)

                  console.log(idColaValorar)
                  valorarAtencion(rate,idColaValorar, function(statusAtencion) {
                    if (statusAtencion == "success"){
                  sendTextMessage(sender,"Gracias por valorar tu atención" )}

                  })
                                                                  })

            }
            if(text=="2"){
              var rate = 2
            console.log(rate)

            revisarMisColas(rutUser, function(misColas,status){

              console.log(misColas)

              console.log(idColaValorar)
              valorarAtencion(rate,idColaValorar, function(statusAtencion) {
                if (statusAtencion == "success"){
              sendTextMessage(sender,"Gracias por valorar tu atención" )}

              })
                                                              })

        }
        if(text=="3"){
          var rate = 3
        console.log(rate)

        revisarMisColas(rutUser, function(misColas,status){

          console.log(misColas)

          console.log(idColaValorar)
          valorarAtencion(rate,idColaValorar, function(statusAtencion) {
            if (statusAtencion == "success"){
          sendTextMessage(sender,"Gracias por valorar tu atención" )}

          })
                                                          })

    }
    if(text=="4"){
      var rate = 4
    console.log(rate)

    revisarMisColas(rutUser, function(misColas,status){

      console.log(misColas)

      console.log(idColaValorar)
      valorarAtencion(rate,idColaValorar, function(statusAtencion) {
        if (statusAtencion == "success"){
      sendTextMessage(sender,"Gracias por valorar tu atención" )}

      })
                                                      })

}
if(text=="5"){
  var rate = 5
console.log(rate)

revisarMisColas(rutUser, function(misColas,status){

  console.log(misColas)

  console.log(idColaValorar)
  valorarAtencion(rate,idColaValorar, function(statusAtencion) {
    if (statusAtencion == "success"){
  sendTextMessage(sender,"Gracias por valorar tu atención" )}

  })
                                                  })

}
}



          //  setMessengerGreeting()
            continue


        }

        //console.log(event.account_linking)

        //console.log(messaging_events.account_linking)

        if (event.account_linking) {
          receivedAccountLink(event);
        }
      //  console.log(event.message.quick_reply)

        if (event.postback) {

          //console.log("Rut Usuario :"+rutUser);

          var text = JSON.stringify(event.postback.payload)
          //var text2 = JSON.stringify(event.message.quick_reply.payload)
          //console.log(idesqueue)
          //console.log(text)



          if(text=="\"hola\""){
            optionServices = []
            optionCompanies = []
            optionBranch = []
            optionQueue =[]

            idesqueue = []
            arrayIdAttention = []

             idSucursal = 0
             idEmpresa = 0
             idCola = 0
             idColaValorar = 0
             id_Attention = 0
             idAttention = 0


            sendTextMessage(sender,"Bienvenido al Bot de Waitless. \n" + "Ingresa tu Rut, sin puntos y con guión (ejemplo: 12345678-9) para comenzar\n" )

            }

            if (idesqueue.length > 0){

              //console.log(text)

              for (var p=0;p<idesqueue.length;p++){

                var idesqueueSelect = idesqueue[p].toString()

                if (text == idesqueueSelect){
                  //console.log(loli)
                  idAttention = arrayIdAttention[p]
                  //console.log(idAttention)
                  idCola = idesqueueSelect.replace("\"","")

                  console.log(idCola)

                  getDatosColas(sender,function(name,current_number,last_number){

                        waiting(sender)
                      })

                }else{

                }


              }

            }



          if(text === "\"payload_1\""){
            revisarRut(rutUser, function(value,status){

              if (status == "waiting"){
                getDatosColas(rutUser, function(name,current_number,last_number) {

                  sendTextMessage(sender,"La cola " + name + " está atendiendo al número: \n "
                  + current_number)
                    });
                verEstadoNumero(rutUser, function(value2){
                sendTextMessage(sender,"Tu número de atención es:\n " + value2 )
              });


/*
            verTiempoEspera(sender, function(value){
              if(value=="No hay numeros"){
                sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
              }
              else if(value=="Indeterminado"){
                sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
              }
              else if(value=="Already attended"){
                sendTextMessage(sender,"Ya fuiste atendido!\n ")
              }
              else{
              sendTextMessage(sender,"Tu tiempo aproximado de espera es:\n " + value + " minutos")
            }
          })*/

          if (idAttention != 0){
            getCodigoQr(sender, function(urlCode){

            sendImageMessage(sender, urlCode)

          })
        }

        }
          else{


          getDatosColas(sender, function(name,current_number,last_number) {

              sendTextMessage(sender,"La cola " + name +  " está atendiendo al número: \n "
              + current_number + " \n Si pides número obtendrás: \n" + last_number )
            });

            verTiempoEspera(sender, function(value){
              if(value=="No hay numeros"){
                sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
              }
              else if(value=="Indeterminado"){
                sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
              }
              else if(value=="Already attended"){
                sendTextMessage(sender,"Ya fuiste atendido!\n ")
              }
              else{
              sendTextMessage(sender,"Tu tiempo aproximado de espera es:\n " + value + " minutos")
            }
          })
            }

            });
          }

          if(text === "\"payload_2\""){

                revisarRut(rutUser, function(value,status){


                if (status=="notattending"){
                  pedirNumeroRenovacion(sender, function(value4) {
                  sendTextMessage(sender,"Tu número de atención es:\n " + value4 )



                  });




                  verTiempoEspera(sender, function(value){
                    if(value=="No hay numeros"){
                      sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
                    }
                    else if(value=="Indeterminado"){
                      sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
                    }
                    else if(value=="Already attended"){
                      sendTextMessage(sender,"Ya fuiste atendido!\n ")
                    }
                    else{
                    sendTextMessage(sender,"Tu tiempo aproximado de espera es:\n " + value + " minutos")
                  }
                });
              }

                else{

                    pedirNumeroRenovacion(sender, function(value4) {
                    sendTextMessage(sender,"Tu número de atención es:\n " + value4 )





                  });
                  verTiempoEspera(sender, function(value){
                    if(value=="No hay numeros"){
                      sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
                    }
                    else if(value=="Indeterminado"){
                      sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
                    }
                    else if(value=="Already attended"){
                      sendTextMessage(sender,"Ya fuiste atendido!\n ")
                    }
                    else{
                    sendTextMessage(sender,"Tu tiempo aproximado de espera es:\n " + value + " minutos")
                  }
                });

                }
              })
              }


          if(text === "\"payload_3\""){
            confirmacionCancelarNumero(sender)
                }
          if(text === "\"payload_4\""){
            sendTextMessage(sender,"Con este bot podrás ver en que número va la fila de Renovación de Beneficios y cuál número podrías pedir.\nPuedes pedir un número y cancelarlo si lo deseas.\n"
            +"Además en el menú puedes reiniciar el bot si no te responde.\nCualquier consulta escríbenos a services@waitless.cl" )
          }

          if(text=="\"payload_5\""){
            revisarMisColas(rutUser, function(misColas,status){


              var ultimaCola = misColas[misColas.length-1]
              console.log(ultimaCola)
              var stateRate = ultimaCola.rate
              console.log(stateRate)
              idColaValorar = ultimaCola.id


            if (stateRate == null  ){
            valorar(sender)
          }
          if (stateRate == 1  ){
          sendTextMessage(sender, "Ya realizaste tu valoración anteriormente")
          }
        if (stateRate == 2  ){
        sendTextMessage(sender, "Ya realizaste tu valoración anteriormente")
        }
        if (stateRate == 3  ){
        sendTextMessage(sender, "Ya realizaste tu valoración anteriormente")
      }
      if (stateRate == 4  ){
      sendTextMessage(sender, "Ya realizaste tu valoración anteriormente")
    }
    if (stateRate == 5  ){
    sendTextMessage(sender, "Ya realizaste tu valoración anteriormente")
  }
            /*else{
              sendTextMessage(sender, "Aún no ha finalizado tu atención para valorarla. Intentalo más tarde")
            }*/
        })


        }


          if(text === "\"cancelar_1\""){
            revisarRut(rutUser, function(value,status){

              if (status == "waiting"){
                cancelarNumero(sender, function(value3){
                  sendTextMessage(sender,"Tu número se ha cancelado.\nPara pedir otro número ve al menú de la esquina inferior izquierda e ingresa a 'Reiniciar' para volver a empezar." )
                });
            }
            })
        }
          if(text === "\"cancelar_2\""){
            revisarRut(rutUser, function(value,status){

            if (status == "waiting"){

              verEstadoNumero(sender, function(value2){
              sendTextMessage(sender,"Tu número de atención es:\n " + value2 )
            });



            verTiempoEspera(sender, function(value){
              if(value=="No hay numeros"){
                sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
              }
              else if(value=="Indeterminado"){
                sendTextMessage(sender,"Tu tiempo aproximado de espera no se puede estimar\n ")
              }
              else if(value=="Already attended"){
                sendTextMessage(sender,"Ya fuiste atendido!\n ")
              }
              else{
              sendTextMessage(sender,"Tu tiempo aproximado de espera es:\n " + value + " minutos")
            }
          });

          }
        })

}
            if(text === "\"cola_1\""){

                    revisarMisColas(rutUser, function(misColas,status){

                      if (misColas.length  > 0 ){

                      var cantMisColas= misColas.length
                      //generarBotonesMisColasEmpresa(sender,cantMisColas,misColas)
                      misColasEmpresas(sender,cantMisColas,misColas)
                     //generarBotonesMisColasEmpresa(sender,cantMisColas,misColas)


                    }


                    else{
                      sendTextMessage(sender, "Aún no has pedido número en una cola. Ve al menú de la esquina inferior izquierda e ingresa a 'Reiniciar' para volver a empezar. ")
                    }
                  })

            }

            if(text === "\"cola_2\""){
              servicios(sender,function(servicios){
                var cantServ= servicios.length
                generarBotonesServicios(sender,cantServ,servicios)

          })
            }
        }


    }

    res.sendStatus(200)
})


const token = "EAADvChHN2mcBABdlOB1BAKj3lOFOvO7y7BV9wVT9Y5gXlGcE6M1fCcX6z5hwhUre8MqOyzX7AzmOsv8l5hPazNnFtyBJQcUFTIvi8DNO3elLZBkZB5TiDZACDwL6Dj0ZBJ0ZAlQGe5k3X21G7Y5gK1V3tk2wdoVYFQNMmqu5OLwZDZD"







/*
function setMessengerGreeting(){

  var messageData = {
    setting_type: "greeting",
    greeting: {
      text: "Hola, chatea conmigo para optimizar tus tiempos de espera en las filas del proceso de Renovación de Beneficios 2017 de la USM!.\n "+
    "Podrás ver el número en que va la fila y la estimación de tiempo para pedir un número.\n "  +
    "Para comenzar pulsa el botón Empezar e ingresa tu rut. "
    }
  }
  callSendAPISetup(messageData);
}*/



//this sets the Get Started button and welcome message



function setWelcomeMessage(){
  addPersistentMenu()
  var messageData = {
    setting_type: "call_to_actions",
    thread_state: "new_thread",
    call_to_actions: [
      {payload: "hola"}
    ]
  }
  callSendAPISetup(messageData);
}

//Sends the messageData for setup
function callSendAPISetup(messageData) {

  request({
    uri: 'https://graph.facebook.com/v2.6/me/thread_settings',
    qs: { access_token: token },
    method: 'POST',
    json: messageData


  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log('response: ' + response.body.result);
    } else {
      console.log('error sending curl');
      console.error(response);
      console.error(error);
    }
  });
}

function callSendAPI(messageData,sender) {

  request({
      url: 'https://graph.facebook.com/v2.6/me/messages',
      qs: {access_token:token},
      method: 'POST',
      json: {
          recipient: {id:sender},
          message: messageData,
      }

  }, function(error, response, body) {
      if (error) {
          console.log('Error sending messages: ', error)
      } else if (response.body.error) {
          console.log('Error: ', response.body.error)
      }
  })
}


var Fn = {
	// Valida el rut con su cadena completa "XXXXXXXX-X"
	validaRut : function (rutCompleto) {
		if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto ))
			return false;

		var tmp 	= rutCompleto.split('-');
		var digv	= tmp[1];
		var rut 	= tmp[0];
		if ( digv == 'K' ) digv = 'k' ;
		return (Fn.dv(rut) == digv );
	},
	dv : function(T){
		var M=0,S=1;
		for(;T;T=Math.floor(T/10))
			S=(S+T%10*(9-M++%6))%11;
		return S?S-1:'k';
	}
}

function revisarRut(sender,callback){
  //console.log(rutUser)

  request({
      url: 'https://www.waitless.cl/queues/'+idCola+'/unique/'+rutUser,

      method: 'GET',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
      var value = jsonArr.return;
      var status = jsonArr.status;
      //console.log(value);
      callback(value,status)

  })

}


// Obtener  todas las colas de los servicios
function servicios(sender,callback) {

request({
    url: 'http://www.waitless.cl/services',
    method: 'GET',
}, function(error, response, body) {
    console.log(body);
    var jsonArr = JSON.parse(body);
console.log(jsonArr.services.length)
  for (var i=0; i<jsonArr.services.length; i++){
    console.log(jsonArr.services[i].name)
  }
  var servicios = jsonArr.services

callback(servicios)

})
}






function pedirNumeroRenovacion(sender,callback) {

  request({
      url: 'https://www.waitless.cl/queues/'+idCola+'/attentions/'+rutUser+'/botFacebook',

      method: 'POST',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
      var value4 = jsonArr.data.value;
      //console.log(value);

      callback(value4)
  })

}

function getCodigoQr(sender,callback){
  console.log(idAttention)
  console.log("https://www.waitless.cl/png/rut/"+rutUser+'/attention/'+idAttention)
  request({
      url: 'https://www.waitless.cl/png/rut/'+rutUser+'/attention/'+idAttention,
      method: 'GET',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
      var urlCode = jsonArr.url;


      callback(urlCode)
  })
}


function sendImageMessage(sender, urlCode) {

  let messageData = {
     "attachment": {
         "type": "image",
         "payload": {

           "url": urlCode


         }
    }
  };



  callSendAPI(messageData,sender);
}


function valorarAtencion(rate,idColaValorar,callback) {
console.log(rate)
console.log(idColaValorar)
  request({
      url: 'https://www.waitless.cl/attention/'+idColaValorar+'/rate/'+rate,

      method: 'POST',
  }, function(error, response, body) {
        var jsonArr = JSON.parse(body);
        var statusAtencion = jsonArr.status
        callback(statusAtencion)
  })

}

function verEstadoNumero(sender,callback){

  request({
      url: 'https://www.waitless.cl/queues/'+idCola+'/attentions/'+rutUser,
      method: 'GET',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
      var value2 = jsonArr.value;
      //console.log(value);

      callback(value2)
  })


}

function verTiempoEspera(sender,callback){
  request({
      url: 'https://www.waitless.cl/queues/'+idCola+'/waiting/'+rutUser,
      method: 'GET',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
      var value = jsonArr.time_stimate_rut;
      //console.log(value);
      callback(value)
  })

}



function cancelarNumero(sender,callback){
  request({
      url: 'https://www.waitless.cl/queues/'+idCola+'/numbers/'+rutUser,
      method: 'POST',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
      var value3 = jsonArr.data;
      //console.log(value);

      callback(value3);
  })
}



function sendTextMessage(sender, text) {

    let messageData = { text:text}

    callSendAPI(messageData,sender)
}

function addPersistentMenu(){
 request({
    url: 'https://graph.facebook.com/v2.6/me/thread_settings',
    qs: { access_token: token },
    method: 'POST',
    json:{
        setting_type : "call_to_actions",
        thread_state : "existing_thread",
        call_to_actions:[
            {
              type:"postback",
              title:"Ayuda",
              payload:"payload_4"
            },
            {
              type:"postback",
              title:"Reiniciar",
              payload:"hola"
            },
            {
              type:"web_url",
              title:"Sitio Web Waitless",
              url:"http://waitless.cl"
            }
          ]
    }

}, function(error, response, body) {
    console.log(response)
    if (error) {
        console.log('Error sending messages: ', error)
    } else if (response.body.error) {
        console.log('Error: ', response.body.error)
    }
})

}

function removePersistentMenu(){
 request({
    url: 'https://graph.facebook.com/v2.6/me/thread_settings',
    qs: { access_token: token },
    method: 'POST',
    json:{
        setting_type : "call_to_actions",
        thread_state : "existing_thread",
        call_to_actions:[ ]
    }

}, function(error, response, body) {
    console.log(response)
    if (error) {
        console.log('Error sending messages: ', error)
    } else if (response.body.error) {
        console.log('Error: ', response.body.error)
    }
})
}

function confirmacionCancelarNumero(sender){
  let messageData = {
     "attachment": {
         "type": "template",
         "payload": {
             "template_type": "generic",
             "elements": [{
                 "title": "¿Estás seguro de cancelar tu número actual?",


                 "buttons": [{
                     "type": "postback",
                     "title": "Sí quiero cancelarlo",
                     "payload": "cancelar_1",
                 }, {
                     "type": "postback",
                     "title": "No quiero cancelarlo",
                     "payload": "cancelar_2",
                 }],


             }],
         }
     }
   }

  callSendAPI(messageData,sender)


}

function opcionBienvenida(sender){
  let messageData = {
     "attachment": {
         "type": "template",
         "payload": {
             "template_type": "generic",
             "elements": [{
                 "title": "¿Qué deseas hacer?",


                 "buttons": [{
                     "type": "postback",
                     "title": "Revisar mis colas",
                     "payload": "cola_1",
                 }, {
                     "type": "postback",
                     "title": "Ver otras colas",
                     "payload": "cola_2",
                 }],


             }],
         }
     }
   }

  callSendAPI(messageData,sender)


}
// Generar botones de todas las colas de los servicios
function generarBotonesServicios(sender,cantServ,servicios) {

  var  payload = ["ser_1","ser_2","ser_3","ser_4","ser_5","ser_6","ser_7","ser_8","ser_9","ser_10"];
  var  options = [];

    for(var i = 0; i < cantServ; i++) {
        options.push({"content_type": "text","title":servicios[i].name, payload:payload[i]});
        optionServices.push({"name":servicios[i].name, "id":servicios[i].id})
      }

    var messageData = {
          "text": "¿En que servicio te gustaría pedir un número de atención?",
          "quick_replies":options
        }

    callSendAPI(messageData,sender)
  }

function generarBotonesCompanies(sender,cantCompanies,companies) {

  var  payload = ["comp_1","comp_2","comp_3","comp_4","comp_5","comp_6","comp_7","comp_8","comp_9","comp_10"];
  var  options = [];

    for(var i = 0; i < cantCompanies; i++) {
        options.push({"content_type": "text","title":companies[i].name, "image_url":companies[i].imgURL, payload:payload[i]});
        optionCompanies.push({"name":companies[i].name, "id":companies[i].id, "imgURL":companies[i].imgURL})
    }

    var messageData = {
          "text": "¿En que empresa te gustaría pedir un número de atención?",
          "quick_replies":options
        }

    callSendAPI(messageData,sender)
  }

  function generarBotonesSucursales(sender,cantSucursales,sucursales) {

    var  payload = ["sucu_1","sucu_2","sucu_3","sucu_4","sucu_5","sucu_6","sucu_7","sucu_8","sucu_9","sucu_10"];
    var  options = [];

      for(var i = 0; i < cantSucursales; i++) {
          options.push({"content_type": "text","title":sucursales[i].name, payload:payload[i]});
          optionBranch.push({"name":sucursales[i].name, "id":sucursales[i].id, "company_id":sucursales[i].company_id})
      }

      var messageData = {
            "text": "¿En que sucursal te gustaría pedir un número de atención?",
            "quick_replies":options
          }

      callSendAPI(messageData,sender)
    }

  function generarBotonesColas(sender,cantColas,colas) {

      var  payload = ["cola_1","cola_2","cola_3","cola_4","cola_5","cola_6","cola_7","cola_8","cola_9","cola_10"];
      var  options = [];

        for(var i = 0; i < cantColas; i++) {
            options.push({"content_type": "text","title":colas[i].name, payload:payload[i]});
            optionQueue.push({"name":colas[i].name, "id":colas[i].id, "id_branch":colas[i].id_branch})
        }

        var messageData = {
              "text": "¿En que cola te gustaría pedir un número de atención?",
              "quick_replies":options
            }

        callSendAPI(messageData,sender)
      }

      function Card (title, subtitle,boton,payload) {
        this.title = title;
        this.subtitle = subtitle;
        //this.image_url = image_url;
        console.log(payload)
        this.buttons = [{type: "postback", title: boton, payload: payload }];

      }
      function finishDecode(safe) {
        return safe.replace(/&amp;/g, '&')
                   .replace(/&lt;/g, '<')
                   .replace(/&gt;/g, '>')
                   .replace(/&quot;/g, '"')
                   .replace(/&#039;/g, "'");
      }
      //send message back
      function misColasEmpresas(sender, cantMisColas,misColas) {

        var elems = [];

        //var  payload = ["miscolas_1","miscolas_2","miscolas_3","miscolas_4","miscolas_5","miscolas_6","miscolas_7","miscolas_8","miscolas_9","miscolas_10"];
        var i = 0;
        for (var cantMisColas in misColas) {
          if (i > 5) { break; }
        //  console.log(misColas)
          var elem = misColas[cantMisColas];
          //console.log(cantMisColas)



          var title = finishDecode(decodeURIComponent(elem['nameCompany']));
          console.log(title)
          var subtitle = finishDecode(decodeURIComponent(elem['nameSucursal']));
            console.log(subtitle)
            var boton = finishDecode(decodeURIComponent(elem['name']));
              var queur = finishDecode(decodeURIComponent(elem['queue_id']));
              id_Attention = finishDecode(decodeURIComponent(elem['id']));
            //  console.log(id_Attention)
              arrayIdAttention.push(id_Attention)
              //console.log(arrayIdAttention)

              idesqueue.push(JSON.stringify(queur))
              console.log(idesqueue)
          //var image_url = finishDecode(decodeURIComponent(elem['imgURL']));

            var payload = queur

          /*if ('0' in elem['media']) {
          var image_url = finishDecode(decodeURIComponent(elem['media']['0']['urlPreview']));
        } else {image_url = "http://ds4q8c259towh.cloudfront.net/20160415XJlq9-dXcb/dist/img/dtc-ph.png"}*/

          var card = new Card(title, subtitle,boton,payload);
          elems.push(card);

          i++;
        }
        json_response.attachment.payload.elements = elems;
      //  console.log(json_response);

      	var messageData = json_response;
      	request({
      		url: 'https://graph.facebook.com/v2.6/me/messages',
      		qs: {access_token:token},
      		method: 'POST',
      		json: {
      			recipient: {id:sender},
      			message: messageData,
      		}
      	}, function(error, response, body) {
      		if (error) {
      			console.log('Error sending messages: ', error);
      		} else if (response.body.error) {
      			console.log('Error: ', response.body.error);
      		}
      	})
      }

/*function generarBotonesMisColasEmpresa(sender,cantMisColas,misColas) {

    var  payload = ["cola_1","cola_2","cola_3","cola_4","cola_5","cola_6","cola_7","cola_8","cola_9","cola_10"];
    var  opciones = [];
  //  var  options2 = [];

      for(var i = 0; i < cantMisColas; i++) {
          opciones.push({"type": "postback","title":misColas[i].name, payload:payload[i]});
          optionMisQueues.push({"name":misColas[i].name, "id":misColas[i].id})
        //  options2.push({"title":misColas[i].nameCompany, "image_url":misColas[i].imgURL});
      }
      console.log(opciones)
      //console.log(options2)

      var messageData = {
      "attachment": {
      "type": "template",
      "payload": {
      "template_type": "generic",
      //"elements":  options2,
      "buttons":  opciones

    }
  }
}




      callSendAPI(messageData,sender)
    }*/

        /*function generarBotonesMisColas(sender,cantMisColas,misColas) {

            var  payload = ["cola_1","cola_2","cola_3","cola_4","cola_5","cola_6","cola_7","cola_8","cola_9","cola_10"];
            var  options = [];

              for(var i = 0; i < cantMisColas; i++) {
                  options.push({"content_type": "text","title":misColas[i].nameCompany, payload:payload[i]});
                  optionMisQueues.push({"id":misColas[i].id, "nameCompany":misColas[i].nameCompany, "queue_id":misColas[i].queue_id})
              }

              var messageData = {
                    "text": "Estas son las colas en las que has pedido número. Selecciona la que quieras revisar",
                    "quick_replies":options
                  }

              callSendAPI(messageData,sender)
            }*/

            function generarBotonesValoracion(sender,cantValoraciones,valoraciones) {

                var  payload = ["cola_1","cola_2","cola_3","cola_4","cola_5","cola_6","cola_7","cola_8","cola_9","cola_10"];
                var  options = [];

                  for(var i = 0; i < cantValoraciones; i++) {
                      options.push({"content_type": "text","title":[i], payload:payload[i]});
                      optionMisValoraciones.push({"number":[i]})
                  }

                  var messageData = {
                        "text": "Ingresa un número del 1 al 5",
                        "quick_replies":options
                      }

                  callSendAPI(messageData,sender)
                }

//Obtener compañias con un id
function getCompanies(sender,id,callback){
  //console.log("id en  getCompanies");
  //console.log(id);

  request({
      url: 'http://www.waitless.cl/company',
      method: 'GET',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
    var companiesID = []
    for (var i=0; i<jsonArr.companies.length; i++){

      if (jsonArr.companies[i].service == id){
        companiesID.push({"id":jsonArr.companies[i].id ,"service":jsonArr.companies[i].service, "name":jsonArr.companies[i].name, "imgURL":jsonArr.companies[i].imgURL});
      }

    }

    var companies = companiesID
    //console.log(companies)
    callback(companies)

  })
}

//Obtener sucursales con un id
function getSucursales(sender,callback){


  request({
      url: 'http://www.waitless.cl/company/'+idEmpresa+'/branch',
      method: 'GET',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
    var sucursalesID = []
    //var company_id = jsonArr.branch[i].company_id
    for (var i=0; i<jsonArr.branches.length; i++){

      if (jsonArr.branches[i].company_id == idEmpresa){
        sucursalesID.push({"id":jsonArr.branches[i].id ,"company_id":jsonArr.branches[i].company_id, "name":jsonArr.branches[i].name});
      }

    }

    var sucursales = sucursalesID
  //  console.log(sucursales)
    callback(sucursales)


  })
}



function getColas(sender,callback){


  request({
      url: 'http://www.waitless.cl/company/'+idEmpresa+'/branch/'+idSucursal+'/queue',
      method: 'GET',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
    var colasID = []

    for (var i=0; i<jsonArr.queue.length; i++){

      if (jsonArr.queue[i].id_branch == idSucursal){

        colasID.push({"id":jsonArr.queue[i].id ,"id_branch":jsonArr.queue[i].id_branch, "name":jsonArr.queue[i].name});
      }

    }

    var colas = colasID
  //  console.log(colas)


    callback(colas)

  })
}

function getDatosColas(sender,callback){
console.log(idCola)
console.log("http://www.waitless.cl/queues/"+idCola)

  request({
      url: 'http://www.waitless.cl/queues/'+idCola,
      method: 'GET',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);

      var name= jsonArr.data.name
      var current_number = jsonArr.data.current_number
      var SLast =  parseInt(jsonArr.data.last_number)
      var last_number = SLast + 1;


    console.log(name)
        console.log(current_number)
            console.log(last_number)


    callback(name,current_number,last_number)

                                      })
                                            }


function revisarMisColas(sender,callback){


  request({
      url: 'http://www.waitless.cl/attentions/'+rutUser,
      method: 'GET',
  }, function(error, response, body) {
      //console.log(body);
      var jsonArr = JSON.parse(body);
      //console.log(jsonArr)
      var misColasID = []
      var statusID =[]


      for (var j=0; j<jsonArr.data.length; j++){

        var status = jsonArr.data[j].status
        var rate = jsonArr.data[j].rate

        if (status == "waiting"){
          var queue_id = jsonArr.data[j].queue_id
          var id = jsonArr.data[j].id

          //ides.push({"id":queue_id });
          statusID.push({"status":status });

          for(var k=0; k<jsonArr.queues.length; k++){
              if (jsonArr.queues[k].id == queue_id){
                var name = jsonArr.queues[k].name
                var branch_id = jsonArr.queues[k].id_branch

              }
          }

          for(var l=0; l<jsonArr.branches.length; l++){
              if (jsonArr.branches[l].id == branch_id){
                var nameSucursal= jsonArr.branches[l].name
                var company_id = jsonArr.branches[l].company_id //esta en string pasarlo a int?

              }
          }

          for(var a=0; a<jsonArr.companies.length; a++){
            if (jsonArr.companies[a].id == company_id){
              var nameCompany = jsonArr.companies[a].name
              var imgURL = jsonArr.companies[a].imgURL


              misColasID.push({"id":id,"rate":rate, "name":name ,"nameSucursal":nameSucursal, "nameCompany":nameCompany,"queue_id":queue_id, "imgURL": imgURL});

            }
          }
                                }
                              }


     var misColas = misColasID
     var status = statusID
     console.log(misColas)
     //console.log(status)
     callback(misColas,status)

  })

}

function valorar(sender) {
    var messageData = {
            "text": "Selecciona un número del 1 al 5. Donde 1 corresponde a una mala atención y 5 una excelente atención",
            "quick_replies":[
                {
                    "content_type":"text",
                    "title": "1",
                    "payload": "valorar_1"
                },
                {
                    "content_type": "text",
                    "title": "2",
                    "payload": "valorar_2"
                },
                {
                    "content_type": "text",
                    "title": "3",
                    "payload": "valorar_3"
                },
                {
                    "content_type": "text",
                    "title": "4",
                    "payload": "valorar_4"
                },
                {
                    "content_type": "text",
                    "title": "5",
                    "payload": "valorar_5"
                }
            ]

    }

    callSendAPI(messageData,sender)
}

function waiting(sender) {

  let messageData = {
     "attachment": {
         "type": "template",
         "payload": {
             "template_type": "generic",
             "elements": [{
                 "title": "Bienvenido a Waitless",
                 "subtitle": "Selecciona un comando",
                 //"image_url": "http://messengerdemo.parseapp.com/img/rift.png",
                 "buttons": [{
                     "type": "postback",
                     "title": "Ver Estado cola",
                     "payload": "payload_1",

                 }, {
                    "type": "postback",
                    "title": "Cancelar Número",
                    "payload": "payload_3",
                 }
               ]
             },

     ]
   }
 }
}


    callSendAPI(messageData,sender)
}

function notattending(sender) {

  let messageData = {
     "attachment": {
         "type": "template",
         "payload": {
             "template_type": "generic",
             "elements": [{
                 "title": "Bienvenido a Waitless",
                 "subtitle": "Selecciona un comando",
                 //"image_url": "http://messengerdemo.parseapp.com/img/rift.png",
                 "buttons": [{
                     "type": "postback",
                     "title": "Ver Estado cola",
                     "payload": "payload_1",

                 }, {
                     "type": "postback",
                     "title": "Pedir Número",
                     "payload": "payload_2",

                 }
               ]
             },

     ]
   }
 }
}


    callSendAPI(messageData,sender)
}

function ready(sender) {

  let messageData = {
     "attachment": {
         "type": "template",
         "payload": {
             "template_type": "generic",
             "elements": [{
                 "title": "Bienvenido a Waitless",
                 "subtitle": "¿te gustaría valorar tu última atención?",
                 //"image_url": "http://messengerdemo.parseapp.com/img/rift.png",
                 "buttons": [{
                   "type": "postback",
                   "title": "Ver Estado cola",
                   "payload": "payload_1"
                 }, {
                     "type": "postback",
                     "title": "Pedir Número",
                     "payload": "payload_2",

                 }]
               },
         {
           "title": "Bienvenido a Waitless",
           "subtitle": "¿te gustaria valorar tu última atención?",
           "buttons": [{
                     "type": "postback",
                     "title": "Valorar Atención",
                     "payload": "payload_5",

                 }
               ]
             },

     ]
   }
 }
}


    callSendAPI(messageData,sender)
}


function sendGenericMessage(sender) {

  let messageData = {
     "attachment": {
         "type": "template",
         "payload": {
             "template_type": "generic",
             "elements": [{
                 "title": "Bienvenido a Waitless",
                 "subtitle": "Selecciona un comando",
                 //"image_url": "https://www.dropbox.com/sh/6knre0o3hpix7rw/AACf6pLcOa8ebteaQwyVe7yYa/Waitless/Logos?dl=0&preview=logo.jpg",
                 "buttons": [{
                     "type": "postback",
                     "title": "Ver Estado cola",
                     "payload": "payload_1",

                 }, {
                     "type": "postback",
                     "title": "Pedir Número",
                     "payload": "payload_2",

                 }
               ]
             },

     ]
   }
 }
}


    callSendAPI(messageData,sender)
}
